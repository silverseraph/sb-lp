local markov = assert(require 'markov')
local stack = assert(require 'stack')

local function printf(s, ...)
    return io.write(s:format(...))
end

local function readl()
    return io.read'*line'
end

local function readkn(k, clear)
    local t = {}

    for i = 1,k do
        t[i] = io.read'*number'
    end

    local clear = clear or true

    if clear then
        io.read'*line'
    end

    return table.unpack(t)
end

local function to_words(suggest)
    local line = suggest
    local pos = 1
    return function()
        local s, e = string.find(line, "%w+", pos)
        if s then
            pos = e + 1
            return string.sub(line, s, e)
        else
            return nil
        end
    end
end

function string:split (sep, max, rex)
    assert(sep ~= '')
    assert(max == nil or max >= 1)

    local record = {}

    if self:len() > 0 then
        local plain = not rex
        max = max or -1

        local field, start = 1, 1
        local first, last = self:find(sep, start, plain)
        while first and max ~= 0 do
            record[field] = self:sub(start, first-1)
            field = field + 1
            start = last + 1
            first, last = self:find(sep, start, plain)
            max = max - 1
        end
        record[field] = self:sub(start)
    end

    return record
end

local function split_to_words(str)
    local t = str:split(' ')

    local s = stack.new()

    for i = 1,#t do s:push(t[(#t-i)+1]) end

    return s
end

local function main()
    printf("Beginning suggestion read-in.  Select your source:\n");
    printf("  0: stdin\n")
    printf("  1: sqlite\n")
    printf("  2: mysql\n")
    printf("  *: quit\n");

    local suggest = {}

    local line = readl()
    local opt = tonumber(line)

    if tostring(opt) ~= line then
        printf("Quitting...\n")
        os.exit(-1)
    end

    if opt == 0 then
        printf("Using STDIN\n")

        while true do
            line = readl()

            if line == '' then break end

            table.insert(suggest, line)
        end
    elseif opt == 1 then
        printf("Using SQLITE\n")

        printf("NOT IMPLEMENTED!\n")

        os.exit(-1)
    else
        printf("Using MySQL\n")

        printf("NOT IMPLEMENTED!\n")

        os.exit(-1)
    end

    local depth = 2

    --printf("Enter the depth of the markov chain that you wish to use: [1-] ");
    --local answer = tonumber(readl())
    --if answer == nil then
        --printf("Invalid answer.  Defaulting to 2\n");
        --depth = answer
    --else
        --depth = answer
        --if depth < 1 then
            --printf("Depth must be greater than 0.  Resetting to 1.\n")
            --depth = 1
        --end
    --end

    m = markov.new(depth)
    m:clean()

    for i = 1,#suggest do
        m:clean()
        --local s = split_to_words(suggest[i])
        --for w in s:iter() do
            --m:insert(w)
        --end
        for w in to_words(suggest[i]) do
            m:insert(w)
        end
        m:insert('*')
    end

    do
        printf("Do you want to see the resultant markov chains? (y/n) ");
        local answer = readl()
        if(answer == 'y' or answer == 'yes') then
            m:show()
        end
    end

    printf("Doing markov generation:\n")
    for i = 1,5 do
        printf("%s\n", m:generate())
    end
end

main()
