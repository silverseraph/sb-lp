local bqueue = {}

bqueue.__index = bqueue

bqueue.new = function(d, default)
    self = {}

    self.depth = d or 2
    if self.depth < 1 then
        self.depth = 1
    end

    self.index = 1
    self.queue = {}
    self.default = default
    for i = 1,self.depth do
        self.queue[i] = self.default
    end

    setmetatable(self, bqueue)

    return self
end

bqueue.push = function(self, item)
    self.queue[self.index] = item
    self.index = (self.index + 1) % self.depth
end

bqueue.__tostring = function(self)
    local t = {}
    for i = 1,self.depth do
        table.insert(t, self.queue[(self.index + i) % self.depth])
        table.insert(t, '|')
    end
    
    return table.concat(t)
end

return bqueue
