local bqueue = assert(require'bqueue')
local random = assert(require'random')

local function markov_printf(s, ...)
    return io.write(s:format(...))
end

local markov = {}
markov.__index = markov

markov.new = function(depth)
    self = {}
    self.depth = depth
    self.history = bqueue.new(self.depth, '*')
    self.chain = {}

    setmetatable(self, markov)

    return self
end

--[[
--  entry
--  {
--      int entries;
--      {} map;
--  }
--]]

markov.insert = function(self, item)
    local key = self.history:__tostring()
    if not self.chain[key] then
        local entry = {}
        entry.entries= 1
        entry.map = {}
        entry.map[item] = 1
        self.chain[key] = entry
    else
        local entry = self.chain[key]
        entry.entries = entry.entries + 1
        if not entry.map[item] then
            entry.map[item] = 1
        else
            entry.map[item] = entry.map[item] + 1
        end
    end

    self.history:push(item)
end

markov.show = function(self, item)
    for k,v in pairs(self.chain) do
        local t = {}
        table.insert(t, '[')
        table.insert(t, '(entries: ' .. tostring(v.entries) .. '), ')
        for u,w in pairs(v.map) do
            table.insert(t, u)
            table.insert(t, ': ')
            table.insert(t, w)
            table.insert(t, ', ')
        end
        table.insert(t, ']')

        markov_printf("%s = %s\n", k, table.concat(t))
    end
end

markov.generate = function(self)
    local hcn = bqueue.new(self.depth, '*')
    local a = random.new()

    local nxt = 'bs value'

    local t = {}

    while nxt ~= '*' do
        local key = tostring(hcn)
        local entry = self.chain[key]
        local rv = math.random()
        --local rv = a()

        for k,v in pairs(entry.map) do
            nxt = k
            if rv <= 0 then
                break
            else
                rv = rv - (v/entry.entries)
            end
        end

        hcn:push(nxt)

        table.insert(t, nxt)
    end

    return table.concat(t, ' ')
end

markov.clean = function(self)
    self.history = bqueue.new(self.depth, '*')
end

return markov
