SHELL := /bin/bash
SRC_DIR = src
BDIR ?= build
BASE = $(shell basename `pwd`)

USE_COLOR ?= -DUSE_COLOR

CC ?= gcc
INCLUDES = $(shell pkg-config --cflags glib-2.0 gsl sqlite3)
LIBS = $(shell pkg-config --libs glib-2.0 gsl sqlite3) -lpthread -lm
CFLAGS += -std=c99 -g -O3 -Wall -Wextra $(USE_COLOR) $(INCLUDES)
CFLAGS_FINAL = $(CFLAGS) $(LIBS)

_DEPS = $(notdir $(wildcard src/*.h))
DEPS = $(patsubst %,$(SRC_DIR)/%,$(_DEPS))

SRC = $(notdir $(wildcard src/*.c))
_OBJ = $(SRC:.c=.o)
OBJ = $(patsubst %,$(BDIR)/%,$(_OBJ))

name = sb-lp 
MAIN = $(name)

.PHONY: default
default: 
	@if [ ! -f btype ]; then echo 'release' >> btype; fi
	@if [ 'release' == `cat btype` ]; then make release; fi
	@if [ 'debug' == `cat btype` ]; then make debug; fi

.PHONY: release release_prep
release_prep: 
	@if [ ! -f btype ]; then echo 'release' >> btype; fi
	@if [ 'debug' == `cat btype` ]; then \
		make clean; \
		echo 'release' >> btype; \
		fi

release: release_prep $(MAIN)

.PHONY: debug debug_prep
debug_prep:
	@if [ ! -f btype ]; then echo 'debug' >> btype; fi
	@if [ 'release' == `cat btype` ]; then \
		make clean; \
		echo 'debug' >> btype; \
		fi

debug: debug_prep
	make $(MAIN) CFLAGS="$(CFLAGS) -DDEBUG"

$(BDIR)/%.o: $(SRC_DIR)/%.c 
	@mkdir -p -- "$(BDIR)"
	$(CC) -c -o $@ $< $(CFLAGS)

$(MAIN): $(OBJ) 
	$(CC) -o $@ $^ $(CFLAGS_FINAL)

.PHONY:
stats:
	if [ -d stats ]; then rm -rf stats; fi
	gitstats . stats

.PHONY:
clean:
	if [ -d stats ]; then rm -rf stats; fi
	if [ -f btype ]; then rm btype; fi
	rm -rf $(BDIR)/ $(MAIN)

