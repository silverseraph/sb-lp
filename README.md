## Suggestion Box - Language Processing

Makes an attempt at filtering massive numbers of suggestions using numerical and statistical techniques like markov chains.  The idea is that you might be able to extract some (mostly) human readable gems from a pile of advice that is less valuable.
