#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"

#ifndef __markov_key_header__
#define __markov_key_header__

typedef struct
{
    int n;
    char **keys;
    int start;
} markov_key;

markov_key *markov_key_new(int n);

void markov_key_free(markov_key *self);

markov_key *markov_key_copy(markov_key *self);

void markov_key_push(markov_key *self, char *tail);

unsigned int markov_key_hash(markov_key *self);

bool markov_key_equal(markov_key *a, markov_key *b);

#endif
