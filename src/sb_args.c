#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include <stddef.h>
#include <stdint.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"
#include "sb_args.h"

sb_args *sb_args_new()
{
    sb_args *self = (sb_args*)malloc(sizeof(sb_args));

    memset(self, 0, sizeof(sb_args));

    self->key_file = NULL;
    self->method = MARKOV;
    self->method_arg = 2;
    self->mode = COUNT;
    self->suggestion_arg = 10;
    self->in_mode = STDIN;
    self->in_arg = NULL;

    return self;
}

void sb_args_free(sb_args *self)
{
    if(self)
    {
        if(self->key_file)
        {
            free(self->key_file);
        }

        free(self);
    }
}

#define OPT_WO_ARG_ERROR(name) log_err("The option " name " requires an argument.\n")

bool sb_args_parse(sb_args *self, int argc, char *argv[])
{
    bool show_help = false;

    for(int i = 1; i < argc; i++)
    {
        if(0 == strcmp(argv[i], "-k") || 0 == strcmp(argv[i], "--keyfile"))
        {
            i++;
            if(i < argc)
            {
                self->key_file = strdup(argv[i]);
            }
            else
                OPT_WO_ARG_ERROR("keyfile");
        }
        else if(0 == strcmp(argv[i], "-a") || 0 == strcmp(argv[i], "--method-arg"))
        {
            i++;
            if(i < argc)
            {
                self->method_arg = atoi(argv[i]);
            }
            else
                OPT_WO_ARG_ERROR("method-arg");
        }
        else if(0 == strcmp(argv[i], "-m") || 0 == strcmp(argv[i], "--method"))
        {
            i++;
            if(i < argc)
            {
                char *lowered = strdup(argv[i]);
                for(gulong j = 0; lowered[j] != 0; j++) lowered[j] = (char)tolower(lowered[j]);

                if(0 == strcmp(lowered, "markov"))
                {
                    self->method = MARKOV;
                }
                else if(0 == strcmp(lowered, "penalized_markov"))
                {
                    self->method = PENALIZED_MARKOV;
                }
                else
                {
                    log_err("Unrecognized argument \"%s\" to option \"method\".  Defaulting to \"markov\"\n", lowered);
                }

                free(lowered);
            }
            else
                OPT_WO_ARG_ERROR("method");
        }
        else if(0 == strcmp(argv[i], "-s") || 0 == strcmp(argv[i], "--suggestion-mode"))
        {
            i++;
            if(i < argc)
            {
                char *lowered = strdup(argv[i]);
                for(gulong j = 0; lowered[j] != 0; j++) lowered[j] = (char)tolower(lowered[j]);

                if(0 == strcmp(lowered, "count"))
                {
                    self->mode= COUNT;
                }
                else if(0 == strcmp(lowered, "interactive"))
                {
                    self->mode = INTERACTIVE;
                }
                else
                {
                    log_err("Unrecognized argument \"%s\" to option \"suggestion-mode\".  Defaulting to \"COUNT\"\n", lowered);
                }

                free(lowered);
            }
            else
                OPT_WO_ARG_ERROR("suggestion-mode");
        }
        else if(0 == strcmp(argv[i], "-c") || 0 == strcmp(argv[i], "--suggestion-arg"))
        {
            i++;
            if(i < argc)
            {
                self->suggestion_arg = atoi(argv[i]);
            }
            else
                OPT_WO_ARG_ERROR("suggestion-arg");
        }
        else if(0 == strcmp(argv[i], "-h") || 0 == strcmp(argv[i], "--help"))
        {
            show_help = true;
        }
        else if(0 == strcmp(argv[i], "-i") || 0 == strcmp(argv[i], "--input"))
        {
            i++;
            if(i < argc)
            {
                char *lowered = strdup(argv[i]);
                for(gulong j = 0; lowered[j] != 0; j++) lowered[j] = (char)tolower(lowered[j]);

                if(0 == strcmp(lowered, "sqlite"))
                {
                    self->in_mode = SQLITE_DB;
                }
                else if(0 == strcmp(lowered, "mysql"))
                {
                    self->in_mode = MYSQL_DB;
                }
                else if(0 == strcmp(lowered, "arg"))
                {
                    self->in_mode = ARG;
                }
                else if(0 == strcmp(lowered, "stdin"))
                {
                    self->in_mode = STDIN;
                }
                else
                {
                    log_err("The input method, \"%s\" is not accepted.  Defaulting to \"stdin\".\n", lowered);
                    self->in_mode = STDIN;
                }
                free(lowered);
            }
            else
                OPT_WO_ARG_ERROR("input");
        }
        else if(0 == strcmp(argv[i], "-o") || 0 == strcmp(argv[i], "--input-arg"))
        {
            i++;
            if(i < argc)
            {
                self->in_arg = strdup(argv[i]);
            }
            else
                OPT_WO_ARG_ERROR("input-arg");
        }
        else
        {
            log_err("Unrecognized argument: \"%s\".  Ignoring in hopes of a better turnout...\n", argv[i]);
        }
    }

    if(show_help)
    {
        sb_args_show_help(argv[0]);
        return false;
    }

    return true;
}

void sb_args_show_help(char *name)
{
    printf("Usage: %s [args...]\n", name);
    printf("        -k || --keyfile         [PATH]                        \n");
    printf("        -a || --method-arg      [INT]                         \n");
    printf("        -m || --method          (MARKOV | PENALIZED_MARKOV)   \n");
    printf("        -s || --suggestion-mode (COUNT | INTERACTIVE)         \n");
    printf("        -c || --suggestion-arg  [INT]                         \n");
    printf("        -i || --input           (SQLITE | MYSQL | ARG | STDIN)\n");
    printf("        -o || --input-arg       [STRING]                      \n");
    printf("        -h || --help                                          \n");
}
