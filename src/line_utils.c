#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"
#include "line_utils.h"

GRegex *split_regex = NULL;

char **split_to_words(char *line)
{
    if(!split_regex)
    {
        GError *err;
        split_regex = g_regex_new(SPLIT_REGEX, G_REGEX_OPTIMIZE, 0, &err);

        if(err)
        {
            log_fubar("%s\n", err->message);
            return NULL;
        }
    }

    return g_regex_split(split_regex, line, 0);
}

char *get_line()
{
    char *line = NULL;
    size_t size = 0;

    if(getline(&line, &size, stdin) < 0)
        return line;
    else
        return NULL;
}
