#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"
#include "markov_value.h"

markov_value *markov_value_new()
{
    markov_value *self = (markov_value*)malloc(sizeof(markov_value));
    self->entries = 0.0;
    self->map = g_hash_table_new_full(
                    (GHashFunc)str_hash,
                    (GEqualFunc)str_equal,
                    NULL,
                    NULL);

    return self;
}

void markov_value_free(markov_value *self)
{
    if(self)
        free(self);
}

void markov_value_increment(markov_value *self, char *key)
{
    unsigned long int value = 0;
    if(!g_hash_table_lookup_extended(self->map, key, NULL, (gpointer*)value))
    {
        value = 0; //if the value wasn't there before, set value to 0
    }

    //insert incremented value into the map
    g_hash_table_insert(self->map, key, (gpointer)value++);

    self->entries++;
}

char *markov_value_suggest(markov_value *self)
{
    double rand = g_random_double();

    if(rand == 1.0) rand = 0.0;

    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init(&iter, self->map);
    while(g_hash_table_iter_next(&iter, &key, &value))
    {
        if(rand <= ((unsigned long int)value)/(self->entries))
        {
            return (char*)key;
        }
    }

    return NULL;
}

char *markov_value_suggest_with_rand(markov_value *self, GRand *rng)
{
    double rand = g_rand_double(rng);

    if(rand == 1.0) rand = 0.0;

    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init(&iter, self->map);
    while(g_hash_table_iter_next(&iter, &key, &value))
    {
        if(rand <= ((unsigned long int)value)/(self->entries))
        {
            return (char*)key;
        }
    }

    return NULL;
}
