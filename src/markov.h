#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"
#include "markov_key.h"

#ifndef __markov_header__
#define __markov_header__

typedef struct
{
    int depth;
    bool penalized;

    GHashTable *hash;
    GHashTable *words;

    markov_key *current;
} markov;

markov *markov_new(int depth, bool penalized);

void markov_free(markov *self);

void markov_push(markov *self, char *word);

char *markov_generate(markov *self);

#endif
