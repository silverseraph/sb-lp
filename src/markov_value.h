#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"

#ifndef __markov_value_header__
#define __markov_value_header__

typedef struct
{
    GHashTable *map;
    double entries;
} markov_value;

markov_value *markov_value_new();

void markov_value_free(markov_value *self);

void markov_value_increment(markov_value *self, char *key);

char *markov_value_suggest(markov_value *self);

char *markov_value_suggest_with_rand(markov_value *self, GRand *rng);

#endif
