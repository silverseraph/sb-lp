#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"
#include "markov.h"
#include "markov_key.h"
#include "markov_value.h"

markov *markov_new(int depth, bool penalized)
{
    markov *self = (markov*)malloc(sizeof(markov));

    memset(self, 0, sizeof(markov));

    self->depth = depth;
    self->penalized = penalized;
    self->hash = g_hash_table_new_full(
            (GHashFunc)markov_key_hash,
            (GEqualFunc)markov_key_equal,
            (GDestroyNotify)markov_key_free,
            (GDestroyNotify)markov_value_free);

    self->words = g_hash_table_new_full(
            (GHashFunc)str_hash,
            (GEqualFunc)str_equal,
            NULL,
            free);

    //need to have the empty string inserted
    /*g_hash_table_insert(self->words, NULL, NULL);*/

    self->current = markov_key_new(depth);

    return self;
}

void markov_free(markov *self)
{
    if(self)
    {
        g_hash_table_destroy(self->hash);
        g_hash_table_destroy(self->words);
        markov_key_free(self->current);
        free(self);
    }
}

void markov_push(markov *self, char *word)
{
    char *redir_word = NULL;

    //check for existince of key in words table, if it's not there, put it there
    if(!g_hash_table_lookup_extended(self->words, word, NULL, (gpointer*)&redir_word))
    {
        redir_word = strdup(word);
        g_hash_table_insert(self->words, redir_word, redir_word);
    }

    markov_key *key = NULL;
    markov_value *value = NULL;

    //check if that lead already exists, and if not, create it
    if(!g_hash_table_lookup_extended(self->hash, self->current, (gpointer*)&key, (gpointer*)&value))
    {
        key = markov_key_copy(self->current);
        value = markov_value_new();

        g_hash_table_insert(self->hash, key, value);
    }

    markov_value_increment(value, redir_word);

    //push the key context so that it's ready for the next word entry
    markov_key_push(self->current, redir_word);
}

char *markov_generate(markov *self)
{
    GString *str = g_string_new(NULL);
    char *word = NULL;
    markov_key *key = markov_key_new(self->depth);
    markov_value *value = NULL;

    GRand *rand = g_rand_new();

    while(1)
    { 
        //if lookup for the current key fails, youre in an invalid state
        if(!g_hash_table_lookup_extended(self->hash, key, NULL, (gpointer*)&value)) break;

        word = markov_value_suggest_with_rand(value, rand);

        if(word == NULL)
        {
            g_string_append_c(str, '.');
            break;
        }
        else
        {
            g_string_append(str, word);
            g_string_append_c(str, ' ');
        }
    }

    return g_string_free(str, false);
}
