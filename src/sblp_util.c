#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <complex.h>

#include <glib-2.0/glib.h>
#include <mysql/mysql.h>

#include "sblp_util.h"

unsigned int str_hash(char *str)
{
    if(str)
        return g_str_hash(str);
    else
        return 0;
}

bool str_equal(char *a, char *b)
{
    if(a == b)
        return true;
    if(a == NULL && b != NULL)
        return false;
    if(b == NULL && a != NULL)
        return false;
    return g_str_equal(a, b);
}
