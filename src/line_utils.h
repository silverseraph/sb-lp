#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"

#ifndef __line_utils_header__
#define __line_utils_header__

extern GRegex *split_regex;

#define SPLIT_REGEX ""

char **split_to_words(char *line);

char *get_line();

#endif
