#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"
#include "markov_key.h"

markov_key *markov_key_new(int n)
{
    markov_key *self = (markov_key*)malloc(sizeof(markov_key));
    self->n = n;
    self->start = 0;

    self->keys = (char**)malloc(sizeof(char*) * self->n);

    for(int i = 0; i < n; i++)
        self->keys[i] = NULL;

    return self;
}

void markov_key_free(markov_key *self)
{
    if(self)
    {
        free(self->keys);
        free(self);
    }
}

markov_key *markov_key_copy(markov_key *self)
{
    markov_key *copy = (markov_key*)malloc(sizeof(markov_key));
    memcpy(copy, self, sizeof(markov_key));

    return copy;
}

void markov_key_push(markov_key *self, char *tail)
{
    self->keys[self->start] = tail;

    self->start = (self->start + 1) % self->n;
}

unsigned int markov_key_hash(markov_key *self)
{
    unsigned int hash = 0;
    char **keys = self->keys;
    int start = self->start;

    for(int i = 0; i < self->n; i++)
    {
        hash *= 29;
        hash += str_hash(keys[i+start]);
    }

    return hash;
}

bool markov_key_equal(markov_key *a, markov_key *b)
{
    int n = a->n;
    int as = a->start, bs = b->start;
    for(int i = 0; i < n; i++)
    {
        if(0 != strcmp(a->keys[(i+as)%n], b->keys[(i+bs)%n]))
        {
            return false;
        }
    }

    return true;
}
