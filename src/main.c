#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <stddef.h>
#include <stdint.h>
#include <complex.h>

#include <sqlite3.h>

#include "sblp_util.h"
#include "sb_args.h"
#include "markov.h"
#include "line_utils.h"

int main(int argc, char* argv[])
{
    log_info("PID: %d\n", getpid());

    sb_args *args = sb_args_new();

    if(!sb_args_parse(args, argc, argv))
    {
        exit(EXIT_FAILURE);
    }

    markov *m = markov_new(args->method_arg, (args->method == PENALIZED_MARKOV));
    char *line = NULL;
    char **words = NULL;
    int word_idx = 0;

    switch(args->in_mode)
    {
        case STDIN:
            while((line = get_line()) != NULL)
            {
                words = split_to_words(line);

                word_idx = 0;

                if(words[0] == NULL)
                    continue;

                while(words[word_idx] != NULL)
                    markov_push(m, words[word_idx]);

                markov_push(m, NULL);
                g_strfreev(words);
                g_free(line);
            }
            break;
        case ARG:
            words = split_to_words(args->in_arg);
            word_idx = 0;

            if(words[0] == NULL)
                break;

            while(words[word_idx] != NULL)
                markov_push(m, words[word_idx]);

            markov_push(m, NULL);
            g_strfreev(words);
            break;
        case SQLITE_DB:
        case MYSQL_DB:
        default:
            log_err("Selected input mode is not unimplemented.\n");
    }

    switch(args->mode)
    {
        case COUNT:
            for(int i = 0; i < args->suggestion_arg; i++)
            {
                line = markov_generate(m);
                printf("%s\n", line);
                g_free(line);
            }
            break;
        case INTERACTIVE:
            printf("Entering interactive suggestion mode. Enter \"quit\" to exit, or \"Enter\" to continue...\n");
            while(1)
            {
                line = get_line();
                if(0 == strcmp(line, "quit"))
                {
                    g_free(line);
                    break;
                }
                else
                {
                    g_free(line);
                    line = markov_generate(m);
                    printf("%s\n", line);
                    g_free(line);
                }
            }
            break;
        default:
            log_err("Selected suggestion mode is not supported.\n");
    }

    markov_free(m);

    return 0;
}
