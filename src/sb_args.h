#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib-2.0/glib.h>

#include "sblp_util.h"

#ifndef __sb_args_header__
#define __sb_args_header__

typedef enum
{
    MARKOV,
    PENALIZED_MARKOV
} AnalysisMethod;

typedef enum
{
    COUNT,
    INTERACTIVE
} SuggestionMode;

typedef enum
{
    SQLITE_DB,
    MYSQL_DB,
    ARG,
    STDIN
} InputMode;

typedef struct
{
    char *key_file;

    AnalysisMethod method;
    int method_arg;

    SuggestionMode mode;
    int suggestion_arg;

    InputMode in_mode;
    char *in_arg;

} sb_args;

sb_args *sb_args_new();

void sb_args_free(sb_args *self);

bool sb_args_parse(sb_args *self, int argc, char *argv[]);

void sb_args_show_help(char *name);

#endif
